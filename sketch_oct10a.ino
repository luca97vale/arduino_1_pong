#include <TimerOne.h>

#define FLASH_LED 3
#define BUTTON_PIN 2

#define LEFT_LED 4
#define CENTRAL_LED 5
#define RIGHT_LED 7

bool gameStarted;

void setup() {
  Serial.begin(9600);
  Serial.println("Welcome to led pong: press key T3 to start");
  pinMode(BUTTON_PIN, INPUT);
  pinMode(FLASH_LED, OUTPUT);

  pinMode(LEFT_LED, OUTPUT);
  pinMode(CENTRAL_LED, OUTPUT);
  pinMode(RIGHT_LED, OUTPUT);
  
  gameStarted=false;
  attachInterrupt(digitalPinToInterrupt(BUTTON_PIN), startGame, CHANGE);
}

void startGame(){
  Serial.println("Go");
  delayMicroseconds(10000);
  gameStarted = true;
  digitalWrite(FLASH_LED, LOW);
}

void blinky(int ledPin){
  int i;
  digitalWrite(ledPin, HIGH);
  for(i = 0; i < 100; i++)
    delayMicroseconds(10000);
  digitalWrite(ledPin, LOW);
  for(i = 0; i < 100; i++)
    delayMicroseconds(10000);
}

void loop() {
  if(!gameStarted) {
    blinky(FLASH_LED);
  } else {
    digitalWrite(LEFT_LED, LOW);
    digitalWrite(CENTRAL_LED, HIGH);
    digitalWrite(RIGHT_LED, LOW);
    delay(1000);
    digitalWrite(LEFT_LED, HIGH);
    digitalWrite(CENTRAL_LED, LOW);
    digitalWrite(RIGHT_LED, LOW);
    delay(1000);
    digitalWrite(LEFT_LED, LOW);
    digitalWrite(CENTRAL_LED, LOW);
    digitalWrite(RIGHT_LED, HIGH);
    delay(1000);
  }
}
